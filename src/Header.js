/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import {
  faEnvelope,
  faTrash,
  faChevronCircleDown
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "@emotion/styled";
import Dropdown from "./Dropdown";

const centerColFlx = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const TopNavContentWrap = styled.div`
  background-color: ${props => props.theme.colors.shadeOfGrey_1};
  padding: 0.5rem 1.25rem;
  display: flex;
`;

const NavContentWrap = styled.div`
  background-color: ${props => props.theme.colors.shadeOfGrey_3};
  box-shadow: 0.1rem 0.25rem 0.4rem 0.2rem
    ${props => props.theme.colors.shadeOfGrey_1};
  display: flex;
  justify-content: space-between;
  align-items: stretch;
`;
const NavItem = styled.div`
  padding: 1rem;
  border-right: solid 1px ${props => props.theme.colors.shadeOfGrey_1};
  flex: ${props => props.space};
  ${centerColFlx}
`;
const topNavLeft = css`
  display: flex;
  a {
    margin-right: 1.5rem;
  }
  flex: 4;
`;
const subNav = css`
  display: flex;
  a {
    margin-right: 1.5rem;
    ${centerColFlx};
  }
`;
const topNavRigth = css`
  display: flex;
  margin-left: auto;
  justify-content: space-between;
  align-items: stretch;
  flex: 1;
`;
const Circle = styled.a`
  height: 2rem;
  width: 2rem;
  line-height: 2rem;
  text-align: center;
  border-radius: 50%;
  display: inline-block;
  background: ${props => props.theme.colors.shadeOfGrey_2};
  border-radius: 50%;
  display: inline-block;
`;
const TopNav = props => (
  <TopNavContentWrap>
    <nav css={topNavLeft}>
      <a href="#" role="button">
        Iste Natus
      </a>
      <a href="#" role="button">
        Explicabo
      </a>
      <a href="#" role="button">
        Omnis
      </a>
      <a href="#" role="button">
        Ipsa Quae
      </a>
      <a href="#" role="button">
        Perspiciatus Unde
      </a>
    </nav>
    <nav css={topNavRigth}>
      <Dropdown width={"10em"} title={"Veris Veritatis"} />
      <Circle href="#">Z</Circle>
    </nav>
  </TopNavContentWrap>
);
const Nav = props => (
  <NavContentWrap>
    <NavItem space={0}>
      <a href="#" css={theme => ({ color: theme.colors.secondary })}>
        Sunt
      </a>
    </NavItem>
    <NavItem space={0}>
      <a href="#" css={centerColFlx}>
        <FontAwesomeIcon icon={faChevronCircleDown} />
        Beata Vitae
      </a>
    </NavItem>
    <NavItem space={4}>
      <a href="#">Vehicula ipsum a arcu cursus. Eget nunc scelerisque</a>
    </NavItem>
    <NavItem space={2} />
    <NavItem space={3}>
      <nav css={subNav}>
        <a href="#">
          <FontAwesomeIcon icon={faTrash} />
          Archieto
        </a>
        <a href="#">Aperiam</a>
        <a href="#">Totam</a>
        <a href="#">
          <FontAwesomeIcon icon={faEnvelope} />
          Rem
        </a>
      </nav>
    </NavItem>
    <NavItem space={1} />
  </NavContentWrap>
);
const Header = () => (
  <header>
    <TopNav />
    <Nav />
  </header>
);

export default Header;
