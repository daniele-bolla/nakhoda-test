/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import styled from "@emotion/styled";
import Dropdown from "./Dropdown";

const BoxContentWrapper = styled.div`
  backgound-color: ${props => props.theme.colors.shadeOfGrey_4};
  box-shadow: 0.1rem 0.25rem 0.4rem 0.2rem
    ${props => props.theme.colors.shadeOfGrey_1};
  border-radius: 1.5rem;
  padding: 1rem 1.5rem;
`;
const List = styled.ul`
  min-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding-bottom: 2rem;
  border: soild 1px #fff;
`;
const NavBoxContentWrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2rem;
  > * {
    flex: 1;
    margin-left: 1rem;
  }
  a {
    display: flex;
    position: relative;
    flex-direction: column-reverse;
    align-items: center;
  }
  a:before {
    content: "";
    left: 0rem;
    bottom: -0.25rem;
    height: 1px;
    width: 0%;
    border-bottom: 2px solid ${props => props.theme.colors.primary};
    transition: width 0.3s;
  }
  a:hover:before {
    width: 20%;
  }
`;
const Main = () => (
  <main
    css={css`
      max-width: 960px;
      margin: 3rem auto;
      padding: 1rem 3rem;
    `}
  >
    <h1
      css={css`
        font-size: 2.5rem;
      `}
    >
      Csed ut perspiciatis unde
    </h1>
    <div
      css={css`
        padding: 0.5rem 0 2rem;
      `}
    >
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at
        ornare nisi. Interdum et malesuada fames ac ante ipsum primis in
        faucibus. Sed maximus consectetur turpis. Nullam aliquet sem sed ante
        volutpat, nec dapibus dolor tempus.
      </p>
    </div>
    <div
      css={css`
        display: flex;
        flex-wrap: wrap;
      `}
    >
      <List>
        <li>Perspiciatus Unde</li>
        <li>Ipsa Quae</li>
        <li>Iste Natus</li>
        <li>Explicabo</li>
        <li>Omnis</li>
      </List>
      <List>
        <li>Perspiciatus Unde</li>
        <li>Ipsa Quae</li>
        <li>Iste Natus</li>
        <li>Explicabo</li>
        <li>Omnis</li>
      </List>
      <List>
        <li>Omnis</li>
      </List>
      <List>
        <li>Perspiciatus Unde</li>
        <li>Ipsa Quae</li>
        <li>Iste Natus</li>
        <li>Explicabo</li>
        <li>Omnis</li>
      </List>
      <List>
        <li>Perspiciatus Unde</li>
        <li>Ipsa Quae</li>
        <li>Iste Natus</li>
        <li>Explicabo</li>
        <li>Omnis</li>
      </List>
    </div>
    <Box />
  </main>
);
const Box = () => (
  <BoxContentWrapper>
    <NavBoxContentWrap>
      <a href="#" css={theme => ({ color: theme.colors.secondary })}>
        Accusatuium dolorem laudant
      </a>
      <a href="#">Dolorem laudant</a>
      <a href="#">audant</a>
    </NavBoxContentWrap>
    <NavBoxContentWrap>
      <a href="#" css={theme => ({ color: theme.colors.secondary })}>
        Accusatuium dolorem laudant
      </a>
      <Dropdown
        css={css`
          border-radius: 0.8rem 0.8rem 0 0;
        `}
        title={"Accusatuium dolorem laudant"}
        darkbg
      />
      <a href="#">Dolorem laudant</a>
    </NavBoxContentWrap>
  </BoxContentWrapper>
);

export default Main;
