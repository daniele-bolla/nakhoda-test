/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { useState } from "react";
import styled from "@emotion/styled";
import { faChevronUp, faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const button = css`
  display: inline-block;
  width: 100%;
  border: none;
  text-align: left;
  cursor: pointer;
  -webkit-appearance: none;
  -moz-appearance: none;
  padding: 0.5rem;
  background: transparent;
  display: flex;
  align-items: center;
`;
const iconAlign = css`
  margin-left: auto;
  order: 2;
`;
const DropdownWrapper = styled.div`
  position: relative;
  background-color: ${props => props.theme.colors.shadeOfGrey_1};
  width: ${props => (props.width ? props.width : "100%")};
`;
const DropdownContent = styled.div`
  position: absolute;
  width: 100%;
  left: 0;
  padding-bottom: 2rem;
`;
const DropdownList = styled.ul`
  background: transparent;
  box-shadow: 0.1rem 0.25rem 0.4rem 0.2rem
    ${props => props.theme.colors.shadeOfGrey_1};
  a {
    background-color: ${props => props.theme.colors.shadeOfGrey_4};
    display: block;
    padding: 0.5rem;
    width: 100%;
  }
  a:hover {
    background-color: ${props => props.theme.colors.shadeOfGrey_2};
  }
`;

class Dropdown extends React.Component {
  constructor() {
    super();
    this.state = {
      displayMenu: false
    };
  }
  showDropdownMenu = event => {
    event.preventDefault();
    this.setState({ displayMenu: true }, () => {
      document.addEventListener("click", this.hideDropdownMenu);
    });
  };
  hideDropdownMenu = event => {
    if (this.node.contains(event.target)) {
      return;
    }
    this.setState({ displayMenu: false }, () => {
      document.removeEventListener("click", this.hideDropdownMenu);
    });
  };
  captureNode = node => (this.node = node);
  iconSwitcher = () => (this.state.displayMenu ? faChevronUp : faChevronDown);
  render() {
    return (
      <DropdownWrapper width={this.props.width} {...this.props}>
        <button
          css={button}
          onClick={e => {
            this.showDropdownMenu(e);
          }}
        >
          {this.props.title}
          <FontAwesomeIcon css={iconAlign} icon={this.iconSwitcher()} />
        </button>
        {this.state.displayMenu ? (
          <DropdownContent ref={this.captureNode}>
            <DropdownList>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
              <li>
                <a href="#Create Page">Architecto Beate Vitae</a>
              </li>
            </DropdownList>
          </DropdownContent>
        ) : null}
      </DropdownWrapper>
    );
  }
}

export default Dropdown;
