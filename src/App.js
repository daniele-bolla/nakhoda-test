/** @jsx jsx */
import { css, jsx, Global } from "@emotion/core";
import { ThemeProvider } from "emotion-theming";
import Header from "./Header";
import Main from "./Main";

const hsl = (hue, saturation, lightness) =>
  `hsl(${hue}, ${saturation}%, ${lightness}%)`;

const shadeOfGrey = lightness => hsl(0, 0, lightness);

const theme = {
  colors: {
    primary: "#d6b14d",
    secondary: "#8E6A5E",
    shadeOfGrey_1: shadeOfGrey(11),
    shadeOfGrey_2: shadeOfGrey(13),
    shadeOfGrey_3: shadeOfGrey(15),
    shadeOfGrey_4: shadeOfGrey(16),
    shadeOfGrey_5: shadeOfGrey(27),
    shadeOfGrey_6: shadeOfGrey(33),
    shadeOfGrey_7: shadeOfGrey(35),
    shadeOfGrey_8: shadeOfGrey(60)
  }
};

const typography = css`
  body {
    font-family: "Open Sans", sans-serif;
    background-color: ${theme.colors.shadeOfGrey_2};
    color: ${theme.colors.shadeOfGrey_8};
  }
  h1 {
    font-family: "Source Sans Pro", sans-serif;
    color: ${theme.colors.primary};
  }
  a,
  button {
    font-size: 1rem;
    color: ${theme.colors.shadeOfGrey_8};
    text-decoration: none;
  }
  a:hover,
  button:hover {
    color: ${theme.colors.primary};
  }
`;
const wrapper = css`
  color: ${theme.colors.shadeOfGrey_8};
  background-color: ${theme.colors.shadeOfGrey13};
`;

const App = () => (
  <ThemeProvider theme={theme}>
    <Global styles={typography} />
    <Body />
  </ThemeProvider>
);

const Wrapper = ({ children }) => <div css={wrapper}>{children}</div>;

const Body = () => (
  <Wrapper>
    <div>
      <Header />
      <Main />
    </div>
  </Wrapper>
);

export default App;
